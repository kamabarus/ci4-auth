<?php

namespace SteeveDroz\CiAuth;

/**
 * This library allows to interact with a model and the session in order to login, logout and check if a user is logged in.
 */
class Auth
{
    /**
     * The model to call in order to check user credentials.
     */
    protected $model;

    /**
     * If the user stored in database is refreshed on each page.
     */
    protected $refresh;

    /**
     * Creates a new authentication library.
     *
     * @param UserModelInterface $modelClass The model that allows to check user credentials. It must implement SteeveDroz\CiAuth\UserModelInterface.
     * @param bool               $refresh    If true (by default), the user in session will be refreshed from the model on each call of the constructor. Else, the session will stay the same even if the user data have changed since login.
     */
    public function __construct(UserModelInterface $model, bool $refresh = true)
    {
        $this->model = $model;
        $this->refresh = $refresh;

        if ($this->refresh) {
            $originalUser = service('session')->get('steevedroz_auth_original_user');
            if (null !== $originalUser) {
                $this->login($originalUser);
            }
        }
    }

    /**
     * Allows to log a user in if their credentials are correct.
     *
     * @param User $user a user that contains all the application-specific data to authenticate it (for example, $user->email and $user->password)
     */
    public function login(array $user): void
    {
        $confirmedUser = $this->model->login($user);
        if (null !== $user) {
            service('session')->set('steevedroz_auth_confirmed_user', $confirmedUser);

            if ($this->refresh) {
                service('session')->set('steevedroz_auth_original_user', $user);
            }
        }
    }

    /**
     * Ends the login session for the currently authenticated user.
     */
    public function logout(): void
    {
        service('session')->remove('steevedroz_auth_confirmed_user');
        service('session')->remove('steevedroz_auth_original_user');
    }

    /**
     * Checks whether a user is logged in.
     *
     * @return bool if a user is logged in
     */
    public function isLoggedIn(): bool
    {
        return null !== service('session')->get('steevedroz_auth_confirmed_user');
    }

    /**
     * Retrieves the authenticated user or one of its fields.
     *
     * @param string $field the field to return or null for the whole user
     *
     * @return mixed the authenticated user if $field is null, the corresponding field if $field is given, or null if no user is authenticated
     */
    public function getUser(?string $field = null)
    {
        $user = service('session')->get('steevedroz_auth_confirmed_user');
        if (null === $user) {
            return null;
        }

        return null === $field ? $user : ($user[$field] ?? null);
    }

    /**
     * Convinient static implementation of Auth::getUser().
     *
     * This requires that an instance of SteeveDroz\CiAuth\Auth is set up as a service called 'auth'.
     *
     * @param string $field the field to return or null for the whole user
     *
     * @return mixed the authenticated user if $field is null, the corresponding field if $field is given, or null if no user is authenticated
     */
    public static function getUserStatic(?array $field = null)
    {
        return service('auth')->getUser($field[0] ?? null);
    }
}
