<?php

namespace SteeveDroz\CiAuth\Mocks;

use SteeveDroz\CiAuth\UserModelInterface;

class UserModelParam implements UserModelInterface
{
    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function login(array $user): ?array
    {
        return $this->user;
    }
}
