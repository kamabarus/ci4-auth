<?php

namespace SteeveDroz\CiAuth\Mocks;

use SteeveDroz\CiAuth\UserModelInterface;

class UserModelInvalid implements UserModelInterface
{
    public function login(array $user): ?array
    {
        return null;
    }
}
