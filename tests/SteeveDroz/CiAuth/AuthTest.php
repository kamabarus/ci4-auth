<?php

namespace SteeveDroz\CiAuth;

use PHPUnit\Framework\TestCase;
use SteeveDroz\CiAuth\Mocks\Session;
use SteeveDroz\CiAuth\Mocks\UserModelInvalid;
use SteeveDroz\CiAuth\Mocks\UserModelParam;
use SteeveDroz\CiAuth\Mocks\UserModelValid;

/**
 * @internal
 * @covers \SteeveDroz\CiAuth\Auth
 */
class AuthTest extends TestCase
{
    public function setUp(): void
    {
        $this->modelValid = new UserModelValid();
        $this->modelInvalid = new UserModelInvalid();
        service('session');
    }

    public function testConstructor()
    {
        $auth = new Auth($this->modelValid, false);
        $this->assertInstanceOf(\SteeveDroz\CiAuth\Auth::class, $auth);

        $auth = new Auth($this->modelInvalid, false);
        $this->assertInstanceOf(\SteeveDroz\CiAuth\Auth::class, $auth);
    }

    public function testLoginValid()
    {
        $_SESSION['steevedroz_auth_confirmed_user'] = null;
        $this->assertNull($_SESSION['steevedroz_auth_confirmed_user']);

        $user = [
            'username' => 'usernameValid',
            'password' => 'passwordValid',
        ];

        $auth = new Auth($this->modelValid, false);

        $auth->login($user);
        $this->assertIsArray($_SESSION['steevedroz_auth_confirmed_user']);
        $this->assertArrayHasKey('username', $_SESSION['steevedroz_auth_confirmed_user']);
        $this->assertArrayHasKey('password', $_SESSION['steevedroz_auth_confirmed_user']);
        $this->assertSame('usernameValid', $_SESSION['steevedroz_auth_confirmed_user']['username']);
        $this->assertSame('passwordValid', $_SESSION['steevedroz_auth_confirmed_user']['password']);
    }

    public function testLoginInvalid()
    {
        $_SESSION['steevedroz_auth_confirmed_user'] = null;
        $this->assertNull($_SESSION['steevedroz_auth_confirmed_user']);

        $user = [
            'username' => 'usernameValid',
            'password' => 'passwordValid',
        ];

        $auth = new Auth($this->modelInvalid, false);

        $auth->login($user);
        $this->assertNull($_SESSION['steevedroz_auth_confirmed_user']);
    }

    public function testLogout()
    {
        $_SESSION['steevedroz_auth_confirmed_user'] = [];

        $auth = new Auth($this->modelValid, false);
        $auth->logout();
        $this->assertNull($_SESSION['steevedroz_auth_confirmed_user']);
    }

    public function testLogoutWithRefresh()
    {
        $_SESSION['steevedroz_auth_confirmed_user'] = [];
        $_SESSION['steevedroz_auth_original_user'] = [];

        $auth = new Auth($this->modelValid);
        $auth->logout();
        $this->assertNull($_SESSION['steevedroz_auth_confirmed_user']);
        $this->assertNull($_SESSION['steevedroz_auth_original_user']);
    }

    public function testIsLoggedInFalse()
    {
        $_SESSION['steevedroz_auth_confirmed_user'] = null;
        $auth = new Auth($this->modelValid, false);

        $this->assertFalse($auth->isLoggedIn());
    }

    public function testIsLoggedInTrue()
    {
        $_SESSION['steevedroz_auth_confirmed_user'] = [];
        $auth = new Auth($this->modelValid, false);

        $this->assertTrue($auth->isLoggedIn());
    }

    public function testGetUserNull()
    {
        $_SESSION['steevedroz_auth_confirmed_user'] = null;
        $auth = new Auth($this->modelValid, false);

        $this->assertNull($auth->getUser());
    }

    public function testGetUserValid()
    {
        $user = [
            'email' => 'email@example.com',
        ];
        $_SESSION['steevedroz_auth_confirmed_user'] = $user;
        $auth = new Auth($this->modelValid, false);

        $this->assertIsArray($auth->getUser());
        $this->assertArrayHasKey('email', $auth->getUser());
        $this->assertSame('email@example.com', $auth->getUser()['email']);
    }

    public function testGetUserAttributeValid()
    {
        $user = [
            'name' => 'John Doe',
            'age' => 42,
            'drivingLicense' => true,
            'height' => 1.72,
            'phone' => '555-1212',
        ];
        $_SESSION['steevedroz_auth_confirmed_user'] = $user;
        $auth = new Auth($this->modelValid, false);

        $this->assertSame('John Doe', $auth->getUser('name'));
        $this->assertSame(42, $auth->getUser('age'));
        $this->assertSame(true, $auth->getUser('drivingLicense'));
        $this->assertSame(1.72, $auth->getUser('height'));
        $this->assertSame('555-1212', $auth->getUser('phone'));
    }

    public function testGetUserAttributeInvalid()
    {
        $user = [];
        $_SESSION['steevedroz_auth_confirmed_user'] = $user;
        $auth = new Auth($this->modelValid, false);

        $this->assertNull($auth->getUser('name'));
        $this->assertNull($auth->getUser('age'));
        $this->assertNull($auth->getUser('drivingLicense'));
        $this->assertNull($auth->getUser('height'));
        $this->assertNull($auth->getUser('phone'));
    }

    public function testGetUserStaticNull()
    {
        $_SESSION['steevedroz_auth_confirmed_user'] = null;

        $this->assertNull(Auth::getUserStatic());
        $this->assertNull(Auth::getUserStatic(['username']));
    }

    public function testGetUserStaticValid()
    {
        $_SESSION['steevedroz_auth_confirmed_user'] = [];

        $this->assertIsArray(Auth::getUserStatic());
        $this->assertNull(Auth::getUserStatic(['username']));
    }

    public function testGetUserStaticAttributeValid()
    {
        $user = [
            'name' => 'John Doe',
            'age' => 42,
            'drivingLicense' => true,
            'height' => 1.72,
            'phone' => '555-1212',
        ];
        $_SESSION['steevedroz_auth_confirmed_user'] = $user;
        $auth = new Auth($this->modelValid, false);

        $this->assertSame('John Doe', Auth::getUserStatic(['name']));
        $this->assertSame(42, Auth::getUserStatic(['age']));
        $this->assertSame(true, Auth::getUserStatic(['drivingLicense']));
        $this->assertSame(1.72, Auth::getUserStatic(['height']));
        $this->assertSame('555-1212', Auth::getUserStatic(['phone']));
    }

    public function testGetUserStaticAttributeInvalid()
    {
        $user = [];
        $_SESSION['steevedroz_auth_confirmed_user'] = $user;
        $auth = new Auth($this->modelValid, false);

        $this->assertNull(Auth::getUserStatic(['name']));
        $this->assertNull(Auth::getUserStatic(['age']));
        $this->assertNull(Auth::getUserStatic(['drivingLicense']));
        $this->assertNull(Auth::getUserStatic(['height']));
        $this->assertNull(Auth::getUserStatic(['phone']));
    }

    public function testRefreshUserNoParam()
    {
        $auth = new Auth(new UserModelParam([
            'name' => 'John Doe',
            'age' => 42,
            'drivingLicense' => true,
            'height' => 1.72,
            'phone' => '555-1212',
        ]));

        $user = [
            'name' => 'John Doe',
        ];

        $auth->login($user);
        $this->assertArrayHasKey('steevedroz_auth_original_user', $_SESSION);
        $this->assertIsArray($_SESSION['steevedroz_auth_original_user']);
        $this->assertCount(1, $_SESSION['steevedroz_auth_original_user']);
        $this->assertArrayHasKey('name', $_SESSION['steevedroz_auth_original_user']);
        $this->assertSame('John Doe', $_SESSION['steevedroz_auth_original_user']['name']);

        $this->assertSame('John Doe', $auth->getUser('name'));
        $this->assertSame(42, $auth->getUser('age'));
        $this->assertTrue($auth->getUser('drivingLicense'));
        $this->assertSame(1.72, $auth->getUser('height'));
        $this->assertSame('555-1212', $auth->getUser('phone'));

        $auth = new Auth(new UserModelParam([
            'name' => 'John Doe',
            'age' => 42,
            'drivingLicense' => true,
            'height' => 1.72,
            'phone' => '555-3434',
        ]));

        $this->assertSame('John Doe', $auth->getUser('name'));
        $this->assertSame(42, $auth->getUser('age'));
        $this->assertTrue($auth->getUser('drivingLicense'));
        $this->assertSame(1.72, $auth->getUser('height'));
        $this->assertSame('555-3434', $auth->getUser('phone'));
    }
}

function service($keyword)
{
    switch ($keyword) {
        case 'session':
            return Session::getInstance();
        case 'auth':
            return new Auth(new UserModelValid(), false);
        default:
            throw new \ErrorException();
    }
}
